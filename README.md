# Categorizer

## Task

Create a simple Categories API that stores category tree to database and returns category parents, children and siblings by category id.

### Requirements
Use Python 3.4+ and Django Framework (or Django Rest Framework).
Use of any other third-party libraries or Django extensions (mptt, treebread, etc) is prohibited.

### Categories Endpoint
Create ​POST `/categories/` ​API endpoint. Endpoint should accept ​json​ body (see example Request​), 
validate input data (see Request) and save categories to database (category name should be unique).

#### Example
Request:
```json
 {
  "name": "Category 1",
  "children": [
    {
      "name": "Category 1.1",
      "children": [
        {
          "name": "Category 1.1.1",
          "children": [
            {"name": "Category 1.1.1.1"},
            {"name": "Category 1.1.1.2"},
            {"name": "Category 1.1.1.3"}
          ]
        },
        {
          "name": "Category 1.1.2",
          "children": [
            {"name": "Category 1.1.2.1"},
            {"name": "Category 1.1.2.2"},
            {"name": "Category 1.1.2.3"}
          ]
        }
      ]
    },
    {
      "name": "Category 1.2",
      "children": [
        {"name": "Category 1.2.1"},
        {
          "name": "Category 1.2.2",
          "children": [
            {"name": "Category 1.2.2.1"},
            {"name": "Category 1.2.2.2"}
          ]
        }
      ]
    }
  ]
}
```
### Category Endpoint
Create ​GET `/categories/<id>/`​ API endpoint. Endpoint should retrieve category name, parents, 
children and siblings (see examples) by primary key (`<id>`) in json format.

#### Example 1

GET `/categories/2/` 

Response:
```json
{
  "id": 2,
  "name": "Category 1.1",
  "parents": [
    {
      "id": 1,
      "name": "Category 1"
    }
  ],
  "children": [
    {
      "id": 3,
      "name": "Category 1.1.1"
    },
    {
      "id": 7,
      "name": "Category 1.1.2"
    }
  ],
  "siblings": [
    {
      "id": 11,
      "name": "Category 1.2"
    }
  ]
}
```
 
#### Example 2

GET `/categories/8/` 

Response:

```json
{
  "id": 8,
  "name": "Category 1.1.2.1",
  "parents": [
    {
      "id": 7,
      "name": "Category 1.1.2"
    },
    {
      "id": 2,
      "name": "Category 1.1"
    },
    {
      "id": 1,
      "name": "Category 1"
    }
  ],
  "children": [],
  "siblings": [
    {
      "id": 9,
      "name": "Category 1.1.2.2"
    },
    {
      "id": 10,
      "name": "Category 1.1.2.3"
    }
  ]
}
```

## Settings (use environ variables)

#### Main app settings
|       Key        |     Value     |   Default   |
|------------------|---------------|-------------|
|`DJANGO_SETTINGS_MODULE`| Django config file. |`categorizer.categorizer.settings`|
|`CATEGORIZER_ALLOWED_HOSTS`| Allowed hosts for django server. Separate by comma. |`"*" (any)`|
|`CATEGORIZER_SECRET_KEY`| Secret key for django site. |`sosecret`|

#### DB settings

|       Key        |     Value     |   Default   |
|------------------|--------------|---------|
|`CATEGORIZER_DB_ENGINE`| Values: `postgresql`, `sqlite`. | `sqlite` |
|`CATEGORIZER_SQLITE_PATH`| `sqlite` filepath. | `./var/db.sqlite3` |
|`CATEGORIZER_POSTGRES_USER`| DB user. | `categorizer` |
|`CATEGORIZER_POSTGRES_PASSWORD`| DB password. | `so_secret` |
|`CATEGORIZER_POSTGRES_DB`| DB name. | `categorizer` |
|`CATEGORIZER_POSTGRES_HOST`| DB host. | `postgres` |
|`CATEGORIZER_POSTGRES_PORT`| DB port. | `5432` |

#### Email settings
|       Key        |     Value     |   Default   |
|------------------|---------------|-------------|
|`CATEGORIZER_EMAIL_HOST`|||
|`CATEGORIZER_EMAIL_PORT`|||
|`CATEGORIZER_EMAIL_HOST_USER`|||
|`CATEGORIZER_EMAIL_HOST_PASSWORD`|||
|`CATEGORIZER_EMAIL_USE_TLS`| Values: `true`, `false`.| `false`|
|`CATEGORIZER_EMAIL_USE_SSL`| Values: `true`, `false`.| `false`|
|`CATEGORIZER_SERVER_EMAIL`|||
|`CATEGORIZER_DEFAULT_FROM_EMAIL`||`CATEGORIZER_EMAIL_HOST_USER`|
 
 
#### Other settings
|       Key        |     Value     |   Default   |
|------------------|---------------|-------------|

