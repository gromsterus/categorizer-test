from django.urls import include, path

from .categories import urls as categories_urls


api_urls = (
    categories_urls.urlpatterns
)

urlpatterns = [
    path('', include(api_urls)),
]
