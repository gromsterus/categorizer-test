from rest_framework import serializers

from categorizer.categories.models import Category


class CreateCategoriesTreeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name', 'children')

    def get_fields(self):
        fields = super().get_fields()
        fields['children'] = self.__class__(many=True, required=False)
        return fields

    def create(self, validated_data):
        return Category.objects.create_tree_from_dict(validated_data)

    def update(self, instance, validated_data):
        raise NotImplementedError()


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name')


class RetrieveCategorySerializer(serializers.ModelSerializer):

    parents = CategorySerializer(many=True, source='relations.parents')
    children = CategorySerializer(many=True, source='relations.children')
    siblings = CategorySerializer(many=True, source='relations.siblings')

    class Meta:
        model = Category
        fields = ('id', 'name', 'parents', 'children', 'siblings')
