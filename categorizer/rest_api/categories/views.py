from rest_framework import mixins, viewsets

from categorizer.categories.models import Category

from .serializers import (
    CreateCategoriesTreeSerializer,
    RetrieveCategorySerializer,
)


class CategoryViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    queryset = Category.objects.all()

    def get_serializer_class(self):
        if self.action == 'create':
            serializer_cls = CreateCategoriesTreeSerializer
        elif self.action == 'retrieve':
            serializer_cls = RetrieveCategorySerializer
        else:
            raise NotImplementedError()

        return serializer_cls
