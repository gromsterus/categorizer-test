import pytest


tree_numerical = {
    'name': 'cat 1',
    'children': [
        {
            'name': 'cat 1.1',
            'children': [
                {'name': 'cat 1.1.1'},
                {'name': 'cat 1.1.2'},
            ],
        },
        {
            'name': 'cat 1.2',
            'children': [
                {'name': 'cat 1.2.1'},
                {
                    'name': 'cat 1.2.2',
                    'children': [
                        {'name': 'cat 1.2.2.1'},
                        {'name': 'cat 1.2.2.2'}]
                },
            ],
        },
    ],
}


@pytest.mark.django_db
class TestCategoriesView:

    def test_post_categories(self, api_client):
        response = api_client.post(
            '/categories/', data=tree_numerical, format='json'
        )
        assert response.status_code == 201

        resp_data = response.json()
        assert 'id' in resp_data
        assert len(resp_data['children']) == 2
        assert 'id' in resp_data['children'][0]

    def test_post_already_exists_categories(self, api_client):
        api_client.post('/categories/', data=tree_numerical, format='json')

        response = api_client.post(
            '/categories/', data=tree_numerical, format='json'
        )
        assert response.status_code == 400

        resp_data = response.json()
        error_msg = 'category with this name already exists.'
        assert resp_data['name'] == [error_msg]
        assert resp_data['children'][0]['name'] == [error_msg]

    def test_get_category(self, api_client):
        response = api_client.post(
            '/categories/', data=tree_numerical, format='json'
        )
        root_id = response.data['id']

        response = api_client.get(f'/categories/{root_id}/')
        expected_keys = {'id', 'name', 'parents', 'children', 'siblings'}

        assert expected_keys == response.json().keys()
