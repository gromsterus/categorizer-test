"""Base django project settings."""
import logging
import os

from django.core.exceptions import ImproperlyConfigured
from django.utils.log import DEFAULT_LOGGING
from django.utils.translation import gettext_lazy as _

from .utils.version import get_version


def _get_parent_dir(path, depth=1):
    """Returns the parent directory."""
    for _ in range(depth + 1):  # noqa
        path = os.path.dirname(path)

    return path


VERSION = get_version()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = _get_parent_dir(os.path.abspath(__file__), depth=2)

_ENV_VAR = 'CATEGORIZER_VAR_PATH'
VAR_PATH = os.getenv(_ENV_VAR, os.path.join(BASE_DIR, 'var'))

SECRET_KEY = os.getenv('CATEGORIZER_SECRET_KEY', 'so_secret')
DEBUG = os.getenv('CATEGORIZER_DEBUG', 'false') == 'true'

ALLOWED_HOSTS = os.getenv('CATEGORIZER_ALLOWED_HOSTS', '*').split(',')

_site_id = os.getenv('CATEGORIZER_SITE_ID', '1')
SITE_ID = None if _site_id.lower() == 'none' else int(_site_id)


# Application definition
INSTALLED_APPS = [
    # Third party apps
    'rest_framework',  # https://github.com/encode/django-rest-framework
    'django_extensions',  # https://github.com/django-extensions/django-extensions # noqa
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    # Local apps
    'categorizer.categories.apps.CategoriesConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'categorizer.categorizer.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'categorizer.categorizer.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

_sqlite = 'sqlite'
_pg = 'postgresql'

_db_engine = os.getenv('CATEGORIZER_DB_ENGINE', _sqlite)
if _db_engine not in (_pg, _sqlite):
    raise ImproperlyConfigured(
        '`CATEGORIZER_DB_ENGINE` have two options `postgresql` and `sqlite`!'
    )


if _db_engine == _sqlite:
    _default_db_path = os.path.join(VAR_PATH, 'db.sqlite3')
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.getenv('CATEGORIZER_SQLITE_PATH', _default_db_path),
        }
    }
else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': os.getenv('CATEGORIZER_POSTGRES_DB', 'categorizer'),
            'USER': os.getenv('CATEGORIZER_POSTGRES_USER', 'categorizer'),
            'PASSWORD': os.getenv(
                'CATEGORIZER_POSTGRES_PASSWORD', 'so_secret'
            ),
            'HOST': os.getenv('CATEGORIZER_POSTGRES_HOST', 'postgres'),
            'PORT': os.getenv('CATEGORIZER_POSTGRES_PORT', '5432'),
        }
    }


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': (
            'django.contrib.auth.password_validation'
            '.UserAttributeSimilarityValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.MinimumLengthValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.CommonPasswordValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.NumericPasswordValidator'
        ),
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en'
LANGUAGES = [
    ('en', _('English')),
]

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale'),
]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(VAR_PATH, 'static')
# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(VAR_PATH, 'media')

for dir_path in (VAR_PATH, STATIC_ROOT, MEDIA_ROOT):
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)


# Email config
_fake = 'fake'
_smtp = 'smtp'

_email_backend = os.getenv('CATEGORIZER_EMAIL_BACKEND', _smtp)
if _db_engine not in (_pg, _sqlite):
    raise ImproperlyConfigured(
        '`CATEGORIZER_EMAIL_BACKEND` have two options `smtp` and `fake`!'
    )

if _email_backend == _smtp:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
elif _db_engine == _fake:
    EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

EMAIL_HOST = os.getenv('CATEGORIZER_EMAIL_HOST', '')
EMAIL_PORT = int(os.getenv('CATEGORIZER_EMAIL_PORT', '25'))
EMAIL_HOST_USER = os.getenv('CATEGORIZER_EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = os.getenv('CATEGORIZER_EMAIL_HOST_PASSWORD', '')
EMAIL_USE_TLS = os.getenv('CATEGORIZER_EMAIL_USE_TLS', 'false') == 'true'
EMAIL_USE_SSL = os.getenv('CATEGORIZER_EMAIL_USE_SSL', 'false') == 'true'
SERVER_EMAIL = os.getenv('CATEGORIZER_SERVER_EMAIL', '')
DEFAULT_FROM_EMAIL = os.getenv(
    'CATEGORIZER_DEFAULT_FROM_EMAIL', EMAIL_HOST_USER
)

# Configure logging
LOGGING = DEFAULT_LOGGING

# Configure nplusone (https://github.com/jmcarp/nplusone/)
NPLUSONE_ON = os.getenv('CATEGORIZER_NPLUSONE_ON', 'false') == 'true'
if NPLUSONE_ON:
    INSTALLED_APPS += ('nplusone.ext.django',)
    MIDDLEWARE.insert(0, 'nplusone.ext.django.NPlusOneMiddleware')

    NPLUSONE_LOGGER = logging.getLogger('nplusone')
    NPLUSONE_LOG_LEVEL = logging.INFO

    LOGGING['loggers']['nplusone'] = {
        'level': NPLUSONE_LOG_LEVEL,
        'handlers': ['console'],
    }

# Configure SQL console log
_off = 'off'
_console = 'console'
LOG_SQL = os.getenv('CATEGORIZER_LOG_SQL', _off)
if LOG_SQL == _console:
    LOGGING['handlers']['sql_console'] = {
        'level': 'DEBUG',
        'class': 'logging.StreamHandler',
    }
    LOGGING['loggers']['django.db.backends'] = {
        'handlers': ['sql_console'],
        'level': 'DEBUG',
    }
