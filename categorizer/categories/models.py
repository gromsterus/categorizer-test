from typing import Any, Dict, List
import copy
import uuid

from django.db import models, transaction
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _


class CategoryManager(models.Manager):
    """Category model manager."""

    def generate_tree_id(self) -> uuid.UUID:
        """Generate unique tree ID."""
        return uuid.uuid4()

    def _create_tree(
        self,
        node: Dict[str, Any],
        tree_id: uuid.UUID,
        level: int,
        parent: 'Category' = None,
    ) -> Dict[str, Any]:
        """Create tree recursive.

        :return: tree with `id` and `instance` objects.

        """
        root = self.create(
            name=node['name'],
            tree_id=tree_id,
            level=level,
            parent=parent,
        )
        node.update({
            'id': root.pk,
            'instance': root,
        })

        for child in node.get('children', []):
            self._create_tree(
                node=child,
                tree_id=tree_id,
                level=level + 1,
                parent=root,
            )
        return node

    @transaction.atomic
    def create_tree_from_dict(self, tree: Dict[str, Any]) -> Dict[str, Any]:
        """Create category tree from dict.

        Example:
            tree = {
                'name': 'cat 1',
                'children': [{'name': 'cat 1.1'}, {'name': 'cat 1.2'}],
            }

        """
        tree_id = self.generate_tree_id()
        new_tree = copy.deepcopy(tree)
        new_tree = self._create_tree(new_tree, tree_id, self.model.ROOT_LEVEL)

        return new_tree

    def get_node_relations(
        self,
        node: 'Category',
    ) -> Dict[str, List['Category']]:
        """Returns relations of node.

        Relations (dict keys):
          * children
          * siblings
          * parents

        """
        relations = {
            'children': [],
            'siblings': [],
            'parents': [],
        }
        tree_qs = (
            self
            .filter(tree_id=node.tree_id, level__lte=node.level + 1)
            .exclude(pk=node.pk)
            .order_by('-level')
        )
        parent_id = node.parent_id
        for rel in tree_qs.iterator():
            if rel.level == (node.level + 1) and rel.parent_id == node.pk:
                relations['children'].append(rel)
            elif rel.level == node.level and rel.parent_id == node.parent_id:
                relations['siblings'].append(rel)
            elif rel.level < node.level and rel.pk == parent_id:
                relations['parents'].append(rel)
                parent_id = rel.parent_id

        return relations


class Category(models.Model):
    """Category."""

    ROOT_LEVEL = 0

    non_cycle_condition = models.Q(parent_id=models.F('id'))

    name = models.CharField(
        _('name'),
        max_length=256,
        unique=True,
    )
    parent = models.ForeignKey(
        on_delete=models.CASCADE,
        to='self',
        verbose_name=_('parent'),
        related_name='children',
        limit_choices_to=non_cycle_condition,
        null=True, blank=True,
    )
    tree_id = models.UUIDField(
        _('tree ID'),
    )
    level = models.SmallIntegerField(
        _('level'),
    )

    objects = CategoryManager()

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

        constraints = (
            models.CheckConstraint(
                check=~models.Q(parent_id=models.F('id')),
                name='non_cycle_check_constraint'
            ),
        )

    def __str__(self) -> str:
        return f'{self.name} {self.tree_id}:{self.level}'

    @cached_property
    def relations(self) -> Dict[str, List['Category']]:
        """Returns category relations."""
        return self.__class__.objects.get_node_relations(self)
