import re

import pytest

from categorizer.categories.models import Category


def category_tree_to_str_array(node: Category) -> list:
    """Creates array of node string representation."""
    str_nodes = []

    depth = node.level

    if depth == 0:
        ext = ''
    else:
        ext = '--' * depth

    str_node = f'{ext}{node.name}'
    str_nodes.append(str_node)

    for child in node.children.all():
        str_nodes.extend(category_tree_to_str_array(child))

    return str_nodes


def tree_to_str(tree: Category) -> str:
    """Creates node string representation.

    Example:
        Cat 1
        --Cat 1.1
        --Cat 1.2
        ----Cat 1.2.1
        ------Cat 1.2.1.1
        ----Cat 1.2.2
        --Cat 1.3

    """
    return '\n'.join(category_tree_to_str_array(tree))


leading_whitespace_re = re.compile(r'^\s+', re.MULTILINE)


def strip_str(text: str) -> str:
    """Trims leading whitespace from the given text tree details."""
    return leading_whitespace_re.sub('', text.rstrip())


def get_by_names(*names):
    return [Category.objects.get(name=name) for name in names]


@pytest.mark.django_db
class TestCategoryModel:

    def test_tree_creation_valid_root(self, simple_tree_tuple):
        simple_tree, _, tree_nodes_count = simple_tree_tuple
        tree = Category.objects.create_tree_from_dict(simple_tree)

        root = tree['instance']
        assert root.level == Category.ROOT_LEVEL, 'Should be root'

        tree_db_count = Category.objects.filter(tree_id=root.tree_id).count()
        assert tree_nodes_count == tree_db_count, 'Should create tree'

    def test_tree_creation_consistency(self, simple_tree_tuple):
        simple_tree, simple_tree_str, _ = simple_tree_tuple

        tree = Category.objects.create_tree_from_dict(simple_tree)
        root = tree['instance']

        assert strip_str(simple_tree_str) == strip_str(tree_to_str(root))

    def test_relations_keys(self, numeric_tree):
        Category.objects.create_tree_from_dict(numeric_tree)

        root_node = Category.objects.get(name='cat 1')
        relations = Category.objects.get_node_relations(root_node)

        assert {'parents', 'children', 'siblings'} == relations.keys()

    def test_relations_root_node_parents(self, numeric_tree):
        Category.objects.create_tree_from_dict(numeric_tree)

        root_node = Category.objects.get(name='cat 1')
        relations = Category.objects.get_node_relations(root_node)

        assert relations['parents'] == []

    def test_relations_root_node_children(self, numeric_tree):
        Category.objects.create_tree_from_dict(numeric_tree)

        root_node = Category.objects.get(name='cat 1')
        relations = Category.objects.get_node_relations(root_node)

        expected_children = get_by_names('cat 1.1', 'cat 1.2')
        assert relations['children'] == expected_children

    def test_relations_root_node_siblings(self, numeric_tree):
        Category.objects.create_tree_from_dict(numeric_tree)

        root_node = Category.objects.get(name='cat 1')
        relations = Category.objects.get_node_relations(root_node)

        assert relations['siblings'] == []

    def test_relations_parents(self, numeric_tree):
        Category.objects.create_tree_from_dict(numeric_tree)

        node = Category.objects.get(name='cat 1.1.1')
        relations = Category.objects.get_node_relations(node)

        expected_parents = get_by_names('cat 1.1', 'cat 1')
        assert relations['parents'] == expected_parents

    def test_relations_children(self, numeric_tree):
        Category.objects.create_tree_from_dict(numeric_tree)

        node = Category.objects.get(name='cat 1.1.1')
        relations = Category.objects.get_node_relations(node)

        expected_children = get_by_names(
            'cat 1.1.1.1', 'cat 1.1.1.2', 'cat 1.1.1.3'
        )
        assert relations['children'] == expected_children

    def test_relations_siblings(self, numeric_tree):
        Category.objects.create_tree_from_dict(numeric_tree)

        node = Category.objects.get(name='cat 1.1.1')
        relations = Category.objects.get_node_relations(node)

        expected_siblings = get_by_names('cat 1.1.2')
        assert relations['siblings'] == expected_siblings
