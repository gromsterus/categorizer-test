import pytest


root_tree = {'name': 'Root'}
root_tree_str = """Root"""
root_tree_nodes_count = 1

simple_tree_1 = {
    'name': 'cat 1',
    'children': [
        {'name': 'cat 1.1',
         'children': [{'name': 'cat 1.1.1',
                       'children': [{'name': 'cat 1.1.1.1'},
                                    {'name': 'cat 1.1.1.2'},
                                    {'name': 'cat 1.1.1.3'}]},
                      {'name': 'cat 1.1.2',
                       'children': [{'name': 'cat 1.1.2.1'},
                                    {'name': 'cat 1.1.2.2'},
                                    {'name': 'cat 1.1.2.3'}]}]},
        {'name': 'cat 1.2',
         'children': [{'name': 'cat 1.2.1'},
                      {'name': 'cat 1.2.2',
                       'children': [{'name': 'cat 1.2.2.1'},
                                    {'name': 'cat 1.2.2.2'}]}]},
    ]
}
simple_tree_1_str = """
cat 1
--cat 1.1
----cat 1.1.1
------cat 1.1.1.1
------cat 1.1.1.2
------cat 1.1.1.3
----cat 1.1.2
------cat 1.1.2.1
------cat 1.1.2.2
------cat 1.1.2.3
--cat 1.2
----cat 1.2.1
----cat 1.2.2
------cat 1.2.2.1
------cat 1.2.2.2
"""
simple_tree_1_nodes_count = 15

simple_tree_2 = {
    'name': 'cat a',
    'children': [
        {'name': 'cat a.a'},
        {'name': 'cat a.b'},
        {
            'name': 'cat a.c',
            'children': [
                {'name': 'cat a.c.a'},
                {'name': 'cat a.c.b'},
                {'name': 'cat a.c.c'},
            ]
        },
    ]
}
simple_tree_2_str = """
cat a
--cat a.a
--cat a.b
--cat a.c
----cat a.c.a
----cat a.c.b
----cat a.c.c
"""
simple_tree_2_nodes_count = 7


@pytest.fixture(params=(root_tree, simple_tree_1, simple_tree_2))
def tree_dict(request):
    return request.param


@pytest.fixture(
    params=(
        (root_tree, root_tree_str, root_tree_nodes_count),
        (simple_tree_1, simple_tree_1_str, simple_tree_1_nodes_count),
        (simple_tree_2, simple_tree_2_str, simple_tree_2_nodes_count)
    ),
)
def simple_tree_tuple(request):
    return request.param


@pytest.fixture
def numeric_tree():
    return simple_tree_1
