from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CategoriesConfig(AppConfig):
    """Categories module config."""

    name = 'categorizer.categories'
    verbose_name = _('Categories')
